---
title: "Big Changes" 
date: 2021-12-07T22:12:20-05:00
draft: false
type: "post" 
---

# Welcome to the Clan Tenno!
First and foremost I wanted to say thank you for those who have stayed with us this long. We are making big changes to our Warframe Clan and Discord to make the expirence more enjoyable. The biggest thing you may have noticed is this very website! That's right we are now putting a face on our community. 

**New Changes:**

 1. [Rules](#rules)
 2. [Events](#events)
 3. [Logo](#logo)
 4. [Build Blogs](#build-blog)

<div id="rules"/>

## Rules

After various incidents we have encounterd we felt that it is best to outline rules and guidelines for being in Bankai. They are not hard to follow please see the [rules](https://waru.pro/rules) section for more details.

<div id="events"/>

## Events

Coming soon to a dojo near you. Clan events with platinum prizes! You will see these added in the updates tab. It will also be located in the Event tab when an event is active. 

<div id="logo"/>

## Logo

We are getting a new logo! Our servers old logo  No longer matches the theme or style of the current warframe clan. Because of these we are designing a new logo to help broaden the apeal of our dojo. 

<div id="builds"/>

## Build Blog
Some of us in the clan like making custom builds. Often times they get lost in discord. So now we are adding a tab to share those very things! If you have a full build for a weapon, pet, warframe feel free to message us! To keep things simple, please use [overframe.gg](https://overframe.gg) for building them. 
