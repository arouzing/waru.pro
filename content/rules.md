---
title: "Rules"
date: 2021-12-08T21:12:40-05:00
draft: false
type: "page"
---

## Now listen here you little shit


1. First and formost follow Discord terms of service. 

2. Grind first, ask later. We love helping out our clan members. However constantly asking for things is not enjoyable for anyone. For anything else there is [warframe.market](https://warframe.market)

3. Be excellent at all times. Respect one another.

4. The [Warframe Wiki](https://warframe.fandom.com/wiki/WARFRAME_Wiki) exists. Please try to look it up here first or [YouTube](https://youtube.com) before asking for extensive help. Quick questions are fine but constant bombarding of everyone with them gets old.

5. Spamming, scamming, stealing and lying can and will result in an **INSTANT BAN** from this server. Honesty is the best policy.

6. Drama will not be tolerated period. If you like instigating fights with other people Twitter exists for that very thing.

7. Playing the victim card is not tollerated. Own up to your mistakes and move on.

8. If the channel is not labled NSFW, do not post any adult content in them. Failure to comply will result in instant deletion of the message and a potential **BAN**.

10. Ignorance is no excuse of the law.

11. If I have to add to this list because of you. **INSTANT BAN**